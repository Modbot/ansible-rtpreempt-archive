This role may be executed by changing to the local-execution directory and running the
Makefile using the instructions in the README.md file there.

Role Name
=========

This is an ansible role that goes with the Bitbucket project, `Real Time Kernel Installation git@bitbucket.org:Modbot/real-time-kernel-installation.git`. Refer to that project for information on how to install a Real Time Kernel on a machine.

Installs
------------
Apt Dependencies:  
- git
- fakeroot
- build-essential
- ncurses-dev
- xz-utils
- libssl-dev
- bc

Patched RT Kernel:  
- Kernel + RT Kernel Patch, with modules and headers
- Grub default kernel set to patched kernel (grub backup created)


Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
