## Purpose

This project installs the real-time preemptable linux kernel on the local machine. It is interactive
in nature because of the need to approve ssh logins. 

Future enhancements will allow it to be run without user intervention and on remote machines. Changing the IP address from ```localhost``` in the `inventory-local` file may be all that is necessary.

Note that the ansible script acts on the `inventory-local` file which starts out as a copy of the `inventory` file. Changes to the `inventory-local` file will not be propagated to the git repository.

## Requirements

You'll need to have Ansible installed on your local machine: ```sudo apt-get -y install ansible```

The machine must have an operating system installed and running and a network connection. Playbooks in this repository are targetted at an Ubuntu operating system and you *MUST* have the following setup:

## Provisioning steps

1. Make sudo passwordless by adding `modbot ALL=(ALL) NOPASSWD:ALL` to the end of `/etc/sudoers.d/90-cloudimg-ubuntu`.
1. Log into Bitbucket using the `humphrey@modbot.com` account. Under User settings (click on icon in bottom left corner of web page) go to SSH keys and add a new key. Paste the contents of `/home/modbot/.ssh/id_rsa.pub` into the key field and label it with the name of the machine you are provisioning.
1. ```make init```
	You must answer 'yes' to the prompts for establishing keys. This step will create the necessary directories and install the Bitbucket repository ```Ansible RTPreempt - git@bitbucket.org:Modbot/ansible-rtpreempt.git``` into ```/etc/ansible/roles/modbot.rtpreempt```
1. ```make install```
1. Install `grub-customizer` by following the instructions here, `http://ubuntuhandbook.org/index.php/2016/04/install-grub-customizer-ubuntu-16-04-lts/`. Once installed, run it and move linux-4.8.15-rt10 to the top.
1. ```shutdown -r now```

If all went well you will reboot into a real-time linux system.
