#!/bin/bash
KERNEL=$1
echo $KERNEL

echo $(awk -v pattern="menuentry .Ubuntu, with Linux  $KERNEL. --class" '$0 ~ pattern' /boot/grub/grub.cfg | sed s/\'//g)

INDEX=""

INDEX+="$(awk '/submenu .Advanced options for Ubuntu./ {print $(NF-1)}' /boot/grub/grub.cfg | sed s/\'//g)"
INDEX+="<"
INDEX+="$(awk -v pattern="menuentry .Ubuntu, with Linux  $KERNEL. --class" '$0 ~ pattern' /boot/grub/grub.cfg | sed s/\'//g)"

echo $INDEX